head(d)

library(nnet)

#
# Foodstamps
sub.d <- get.subset.public.programs(d, 'foodstamps')
summary(sub.d)

fit.foodstamps <- multinom(nr.dr.visits.4cats ~ foodstamps, data=sub.d)
summary(fit.foodstamps)
confint(fit.foodstamps)

fit.foodstamps.full <- multinom(nr.dr.visits.4cats ~ foodstamps + income.level + insurance.type + age + gender + has.asthma + has.limiting.condition +
                                  gen.health.cond + citizen.status.mother + citizen.status + rural.urban + fam.type + household.size, data=sub.d)
summary(fit.foodstamps.full)
confint(fit.foodstamps.full)

fit.foodstamps.final <- multinom(nr.dr.visits.4cats ~ foodstamps + income.level + insurance.type + age + has.asthma + has.limiting.condition +
                                  gen.health.cond + household.size, data=sub.d)
summary(fit.foodstamps.final)
confint(fit.foodstamps.final)



#
# Calworks
sub.d <- get.subset.public.programs(d, 'calworks')
summary(sub.d)


fit.calworks <- multinom(nr.dr.visits.4cats ~ calworks, data=sub.d)
summary(fit.calworks)
confint(fit.calworks)

fit.calworks.final <- multinom(nr.dr.visits.2cats6 ~ calworks + income.level + insurance.type + age + has.asthma + has.limiting.condition+
                           gen.health.cond + household.size , data=sub.d)


summary(fit.calworks.final)
confint(fit.calworks.final)

#
# WIC

sub.d <- get.subset.public.programs(d, 'wic')
summary(sub.d)

fit.wic <- multinom(nr.dr.visits.4cats ~ wic, data=sub.d)
summary(fit.wic)
confint(fit.wic)

fit.wic.full <- multinom(nr.dr.visits.4cats ~ wic + income.level + insurance.type + age + gender + has.asthma + has.limiting.condition +
                                  gen.health.cond + citizen.status.mother + citizen.status + rural.urban + fam.type + household.size, data=sub.d)
summary(fit.wic.full)
confint(fit.wic.full)

fit.wic.final <- multinom(nr.dr.visits.4cats ~ wic + income.level + insurance.type + age + has.asthma + has.limiting.condition +
                                   gen.health.cond + household.size, data=sub.d)
summary(fit.wic.final)
confint(fit.wic.final)

#
# Three public programs

fit.pp <- multinom(nr.dr.visits.4cats ~ foodstamps + calworks + wic, data=d)
summary(fit.pp)
confint(fit.pp)

fit.pp.full <- multinom(nr.dr.visits.4cats ~ foodstamps + calworks + wic + nr.programs + income.level + insurance.type + age + gender + has.asthma + has.limiting.condition +
                           gen.health.cond + citizen.status.mother + citizen.status + rural.urban + fam.type + household.size, data=d)
summary(fit.pp.full)
confint(fit.pp.full)

fit.pp.final <- multinom(nr.dr.visits.4cats ~ foodstamps + calworks + wic + nr.programs + income.level + insurance.type + age + has.asthma + has.limiting.condition +
                            gen.health.cond + citizen.status.mother +  household.size, data=d)
summary(fit.pp.final)
confint(fit.pp.final)


#
#
#

fit.pp.er
fit.pp.er.final <- glm(er.visits ~ foodstamps + calworks + wic + nr.programs + income.level + insurance.type + age + has.asthma + has.limiting.condition +
                           gen.health.cond + citizen.status.mother +  household.size, data=d, family=binomial())
summary(fit.pp.er.final)
